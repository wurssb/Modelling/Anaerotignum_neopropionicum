#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Publication:"Genome-scale metabolic modelling enables deciphering ethanol metabolism
via the acrylate pathway in the propionate-producer Anaerotignum neopropionicum"

Author: Sara Benito Vaquerizo and Rik van Rosmalen

An example script to simulate the fermentation of ethanol in Anaerotignum neopropionicum
through dFBA.

Ethanol, Biomass, Acetate,Propionate,Butyrate and Propanol are modelled as dynamic states.
Ethanol (EX_cpd00363_e0) has the lower bound constrained with Michealis_Menten kinetics.

The script generates an excel file with the concentration profile of the dynamic states.
The data is plotted and saved in a figure (Fig.3).

Cobrapy and functions are used from https://github.com/opencobra/m_model_collection/
Python 3.6.9 is used as the programming language

"""

# Import statements

import cobra
import numpy as np
import pandas as pd
from pandas import DataFrame
from openpyxl import load_workbook
import math
import plotly.graph_objects as go
import plotly.express as px
import matplotlib.pyplot as plt


def plot_data(t1, y1):
    """
    Plot fermentation profile and create a figure.

    """
    bio = []
    eth = []
    ac = []
    prop = []
    proh = []
    but = []
    for i in y1:  # Get values for each dynamic state.
        bio.append(i[1])
        eth.append(i[0])
        ac.append(i[2])
        prop.append(i[3])
        proh.append(i[5])
        but.append(i[4])

    fig, ax = plt.subplots()

    ax.plot(t1, eth, linestyle="solid", color="red", label="{}".format("Ethanol"))
    ax.plot(t1, ac, linestyle="solid", color="green", label="{}".format("Acetate"))
    ax.plot(t1, prop, linestyle="solid", color="black", label="{}".format("Propionate"))
    ax.plot(t1, proh, linestyle="solid", color="orange", label="{}".format("Propanol"))
    ax.plot(t1, but, linestyle="solid", color="purple", label="{}".format("Butyrate"))
    ax2 = ax.twinx()
    ax2.plot(t1, bio, linestyle="solid", color="blue", label="{}".format("Biomass"))

    t_stop = np.isnan(y1)[:, 0].argmax() - 1
    if t_stop != -1:
        eth_extended = [eth[t_stop], eth[t_stop]]
        ac_extended = [ac[t_stop], ac[t_stop]]
        prop_extended = [prop[t_stop], prop[t_stop]]
        proh_extended = [proh[t_stop], proh[t_stop]]
        but_extended = [but[t_stop], but[t_stop]]
        bio_extended = [bio[t_stop], bio[t_stop]]
        t_extended = [t1[t_stop], t1[-1]]
        print(t_extended, eth_extended)
        ax.plot(t_extended, eth_extended, linestyle="solid", color="red", alpha=0.75, label='_')
        ax.plot(t_extended, ac_extended, linestyle="solid", color="green", alpha=0.75, label='_')
        ax.plot(t_extended, prop_extended, linestyle="solid", color="black", alpha=0.75, label='_')
        ax.plot(t_extended, proh_extended, linestyle="solid", color="orange", alpha=0.75, label='_')
        ax.plot(t_extended, but_extended, linestyle="solid", color="purple", alpha=0.75, label='_')
        ax2.plot(t_extended, bio_extended, linestyle="solid", color="blue", alpha=0.75, label='_')

    # Experimental data - Batch cultivations of A. neopropionicum on ethanol.

    t2 = [0, 13, 16, 19, 22, 26, 36, 40, 47, 62, 67, 87]
    Ethanol_e = [23, 21.8, 20.2, 17.7, 15.7, 13.8, 7.9, 4.8, 2.3, 0.6, 0.5, 0.4]
    Biomass_e = [0.00427,0.0097,0.016,0.022,0.028,0.033,0.04,0.042,0.045,0.041,0.0402,0.0407]
    Acetate_e = [0.2, 0.7, 1.1, 1.8, 2.5, 3.2, 5.7, 5.9, 7.3, 8.6, 8.4, 8.6]
    Propionate_e = [0.1, 1.1, 1.9, 2.9, 3.7, 4.4, 6.9, 6.8, 8, 9.3, 9.3, 9.5]
    Propanol_e = [0, 0, 0, 0.1, 0.2, 0.4, 0.9, 0.9, 1.2, 1.4, 1.3, 1.3]
    Butyrate_e = [0, 0, 0, 0.4, 0.3, 0.4, 0.6, 0.6, 0.8, 0.9, 0.9, 0.9]

    stdev_Ethanol_e = [0.6, 0.7, 0.3, 0.3, 0.3, 0.3, 0.3, 0.4, 0.3, 0.9, 0.1, 0.0]
    stdev_Biomass_e = [0.00126,0.00094,0.0012,0.0007,0.0011,0.001,0.0019,0.0017,0.00125,0.00065,0.00018,0.00036]
    stdev_Acetate_e = [0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.2, 0.3, 0.2, 0.1, 0]
    stdev_Propionate_e = [0.0, 0.1, 0.0, 0.1, 0.0, 0.1, 0.1, 0.4, 0.3, 0.1, 0.2, 0.1]
    stdev_Propanol_e = [0, 0, 0, 0, 0, 0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0]
    stdev_Butyrate_e = [0, 0, 0, 0, 0.1, 0.1, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0]

    ax.errorbar(t2,Ethanol_e,stdev_Ethanol_e,linestyle="None",fmt="-o",markersize=5,color="red",alpha=0.7)
    ax.errorbar(t2,Acetate_e,stdev_Acetate_e,linestyle="None",fmt="-o",markersize=5,color="green",alpha=0.7)
    ax.errorbar(t2,Propionate_e,stdev_Propionate_e,linestyle="None",fmt="-o",markersize=5,color="black",alpha=0.7)
    ax.errorbar(t2,Propanol_e,stdev_Propanol_e,linestyle="None",fmt="-o",markersize=5,color="orange",alpha=0.7)
    ax.errorbar(t2,Butyrate_e,stdev_Butyrate_e,linestyle="None",fmt="-o",markersize=5,color="purple",alpha=0.7)
    ax2.errorbar(t2,Biomass_e,stdev_Biomass_e,linestyle="None",fmt="s",markersize=5,color="blue",alpha=0.7)
    ax.set_ylabel("Concentration (mM)")
    ax2.set_ylabel("Biomass (g/L)")
    ax2.set_ylim(0)
    ax.set_xlabel("Time (h)")
    fig.legend(loc="upper left",bbox_to_anchor=(0.0, 0.8),bbox_transform=ax.transAxes,fontsize="x-small")
    fig.set_size_inches(8, 6)    
    fig.savefig("Fig3.png")

    return


def michealis_menten(qmax, km):
    """
    Function to generate a Michaelis-Menten kinetics based rate function.

    """

    def f(s):
        if s <= 0:
            return 0
        return qmax * s / (km + s)

    return f


def setBounds(model, kinetics, y):
    """
    Set bounds on the model based on the defined kinetics and state.

    """
    for reaction_name, metabolite_inputs, function in kinetics:
        reaction = model.reactions.get_by_id(reaction_name)
        # Find the right metabolites as input and calculate each reaction rate.
        # Note the minus here to make sure the lower bound is negative
        # as it is refered to uptakes!
        reaction.lower_bound = -function(y[metabolite_inputs])

    return model


def updateObjective(model, last_flux, ref_flux, weights=(1, 1)):
    """
    Update the FBA objective function based on the current and last solution.

    """
    terms = []
    for reaction in model.reactions:
        # This is the last time steps flux as a pandas dataframe
        f1 = last_flux.loc[reaction.id]
        # This is the flux if optimised without the last time step
        f2 = ref_flux.loc[reaction.id]
        # Get weighted average (This will be the flux level we will optimise towards)
        target = (weights[0] * f1 + weights[1] * f2) / sum(weights)
        # Objective is the minimum squared distance to the reference
        term = (reaction.flux_expression - target) ** 2
        terms.append(term)
    q_objective = model.problem.Objective(sum(terms), direction="min")
    model.objective = q_objective

    return model


def simulate(model,state,state_name,kinetics,dynamics_flux_names,biomass_reaction,
			t_start,t_end,t_step,t_feed,feed_dc,flux_weights,verbose=False):

    """
    Simulate the dFBA model.

    """
    # Preallocate result arrays
    all_t = np.arange(t_start, t_end + t_step, t_step)
    all_y = np.zeros((len(all_t), len(state))) * np.NaN
    all_y[0, :] = state

    if flux_weights:
        last_flux = model.optimize().to_frame().fluxes

    # Get some indeces for later.
    biomass_idx = np.where(np.array(state_name) == "Biomass")[0]

    # If verbose, preprare the formatted strings for the output.
    if verbose:
        print("Time - "+ " ".join(state_name)+ " ("+ " ".join(dynamics_flux_names)+ ")")
        string = ("{:>5.3f} - "+ " ".join(["{:>6.3f}"] * len(state))+ " (" + " ".join(["{:>6.3f}"] * len(dynamics_flux_names))+ ")")

    # Euler integration
    for idx, t in enumerate(all_t[:-1]):
        y = all_y[idx]
        yp = all_y[idx + 1]

        # Apply dynamic bounds
        model = setBounds(model, kinetics, y)
        # Optionally, we can run the simulation to be close to the last step every time.
        # This might be more realistic in some cases where metabolism has to make a switch
        # between different fluxes.
        if flux_weights:
            raise ValueError
            # Normal optimization for biomass
            model.objective = biomass_reaction.flux_expression
            ref_flux = model.optimize().to_frame().fluxes

            # Optimization with last flux and reference flux as target
            model = updateObjective(model, last_flux, ref_flux, weights=flux_weights)
            # Apply dynamic bounds again
            model = setBounds(model, kinetics, y)
            # model = setBounds(model, kinetics, growth_kinetics,y)
            sol = model.optimize()
            if sol.status == "infeasible":
                with model:
                    model.reactions.rxn00062_c0.bounds = (0, 1000)
                    model.objective = "rxn00062_c0"
                    sol = model.optimize(raise_error=True)
                    flux = sol.to_frame().fluxes
            else:
                flux = sol.to_frame().fluxes
            # Save for next time
            last_flux = flux
        # Alternatively, we can calculate the flux the normal (fast) way.
        else:
            sol = model.optimize()
            if sol.status == "infeasible":
                with model:
                    model.reactions.rxn00062_c0.bounds = (0, 1000)
                    model.objective = "rxn00062_c0"
                    sol = model.optimize()
                    if sol.status == "infeasible":
                        return all_t, all_y
                    else:
                        flux = sol.to_frame().fluxes
            else:
                flux = sol.to_frame().fluxes

        # Extract fluxes needed to update state
        dynamic_fluxes = flux.loc[dynamics_flux_names]

        # Calculate new state, don't forget to scale to the time and biomass.
        biomass = y[biomass_idx]
        yp[:] = y + t_step * biomass * dynamic_fluxes.values

        if verbose:
            print(string.format(t, *(y.tolist() + dynamic_fluxes.values.tolist())))
    return all_t, all_y


if __name__ == "__main__":
    model = cobra.io.read_sbml_model("iANEO_SB607.sbml")  # Read the model
    model.reactions.EX_cpd00363_e0.lower_bound = -36.5  # Ethanol uptake
    model.reactions.EX_cpd00363_e0.upper_bound = 0  # -0.05
    model.reactions.EX_cpd00011_e0.lower_bound = -30  # CO2 uptake
    model.reactions.EX_cpd00011_e0.upper_bound = -1
    model.reactions.EX_cpd00084_e0.lower_bound = -0.01
    model.reactions.EX_cpd00084_e0.upper_bound = -0.001
    model.reactions.EX_cpd00048_e0.bounds = [-0.012, -0.000]
    model.reactions.rxn08985_c0.bounds = [-1000, 1000]
    model.reactions.rxn05209_c0.bounds = [-1000, 1000]
    model.reactions.rxn10042_c0.bounds = [-1000, 0]
    model.reactions.EX_cpd03559_e0.lower_bound = 0.6
    model.reactions.EX_cpd03559_e0.upper_bound = 3
    model.reactions.EX_cpd00211_e0.lower_bound = 0.4  # Butyrate production 0.03
    model.reactions.EX_cpd00211_e0.upper_bound = 0.6  # 0.358
    model.reactions.EX_cpd00029_e0.lower_bound = 0.1  # Acetate production
    model.reactions.EX_cpd00029_e0.upper_bound = 8
    model.reactions.EX_cpd00141_e0.lower_bound = 0.8  # Propionate production
    model.reactions.EX_cpd00141_e0.upper_bound = 12
    # model.reactions.get_by_id("BIOMASS_Aneopro_w_GAM").lower_bound = 0.001
    model.reactions.get_by_id("BIOMASS_Aneopro_w_GAM").upper_bound = 0.082  # maximum growth rate
    model.solver = "cplex"#Alternative solvers can also be used

    # State
    state_name = np.array(
        ["Ethanol", "Biomass", "Acetate", "Propionate", "Butyrate", "Propanol"])

    uptake_inhibition = "none"

    state = np.array([23, 0.0055, 0.15, 0.09, 0, 0.0])
    qmax_eth = 36.5

    kinetics = [("EX_cpd00363_e0",np.where(np.array(state_name) == "Ethanol")[0],
            michealis_menten(qmax=qmax_eth, km=11))]

    # Fluxes related to the corresponding defined states.
    dynamics_flux_names = ["EX_cpd00363_e0","BIOMASS_Aneopro_w_GAM","EX_cpd00029_e0", "EX_cpd00141_e0","EX_cpd00211_e0","EX_cpd03559_e0"]

    biomass_reaction = model.reactions.BIOMASS_Aneopro_w_GAM
    model.reactions.rxn00062_c0.lower_bound = 1.9
    # Simulation settings
    # t_end=fermentation time from experiments.
    t_start, t_end, t_step = 0, 87, 0.04
    t_feed = [0, 0]
    feed_dc = 0

    verbose = True

    # No delay
    flux_weights = False
    t1, y1 = simulate(model,state,state_name,kinetics,dynamics_flux_names,biomass_reaction,
        t_start, t_end,t_step,t_feed,feed_dc,flux_weights,verbose=False)

    #plt.ion()
    plot_data(t1, y1)  # To generate a plot with the fermentation profile.
    #plt.show()
    # # Optional, save the dfba in an excel sheet.

    df1 = DataFrame(y1, index=t1, columns=state_name)
    name = "ETOH_" + str(state[0]) + "_" + str(t_end)
    writer = pd.ExcelWriter("dFBA_Etoh.xlsx", engine="openpyxl")
    df1.to_excel(writer, sheet_name=name)
    writer.save()
    writer.close()
