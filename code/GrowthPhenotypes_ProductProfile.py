#!/usr/bin/env python

"""
Publication:"Genome-scale metabolic modelling enables deciphering ethanol metabolism 
via the acrylate pathway in the propionate-producer Anaerotignum neopropionicum"

Author: Sara Benito Vaquerizo

An example script to perform FBA and Flux sampling to calculate growth and product profile on
different carbon sources in A. neopropionicum (Table and Figure 2). 

The script generates an excel file with the average and stdv of the fluxes obtained using Flux sampling
under the specified constraints. 

Cobrapy and functions are used from https://github.com/opencobra/m_model_collection/
Python 3.6.9 is used as the programming language

"""

# Import statements
import os
import warnings
import re
from itertools import chain
from sys import argv

import sympy
import scipy
import scipy.io

import statistics
import cobra
import cobra.test

from cobra import Model, Reaction, Metabolite
import pandas
from pandas import DataFrame
from cobra.util.solver import linear_reaction_coefficients

import numpy as np
import pandas as pd
from contextlib import suppress
from cobra.flux_analysis import (
    single_gene_deletion, single_reaction_deletion, double_gene_deletion,
    double_reaction_deletion)
from datetime import datetime

from cobra.medium import minimal_medium
from cobra.test import create_test_model

import multiprocessing
import multiprocessing.pool
from cobra.flux_analysis import production_envelope
from cobra.flux_analysis import gapfill

from cobra.sampling import OptGPSampler, ACHRSampler, sample
from cobra.flux_analysis import gapfill

import math
from math import sqrt
from cobra.flux_analysis.loopless import add_loopless, loopless_solution
from cobra.flux_analysis import pfba
import statistics as stats
from statistics import stdev
import optlang
import cobra.util.solver as sutil
import plotly.graph_objects as go
import plotly.express as px
from cobra.flux_analysis.loopless import add_loopless, loopless_solution
from cobra.flux_analysis import pfba

def essentiality_test(Model):
	"""
	Calculate the number of essential genes and reactions for a given constraint
	
	Input:
	model: model, cobrapy model structure
	
	Output:
	Number of essential genes and reactions
	"""
	
	deletion_results = single_gene_deletion(model)
	count=0
	for i in range(len(deletion_results.growth)):
		if deletion_results.growth[i]<10E-06 or deletion_results.status[i]!='optimal':
			count=count+1

	deletion_results2 = single_reaction_deletion(model)
	count2=0 
	for i in range(len(deletion_results2.growth)):
		if deletion_results2.growth[i]<10E-06 or deletion_results2.status[i]!='optimal':
			count2=count2+1
	
	return count, count2

	
if __name__ == "__main__":
	model=cobra.io.read_sbml_model("iANEO_SB607.sbml")	 #Read cobra model
	#cobra.io.save_json_model(model, "iANEO_SB607.json") #Save model into json format
	model.reactions.EX_cpd00011_e0.bounds=[-1000,1000]  #Co2 is consumed with ethanol as carbon source (non reversible with alternative carbon source)
	model.reactions.EX_cpd00363_e0.bounds=[-0,-0]
	#model.reactions.EX_cpd00363_e0.bounds=[-30,-30] #Change reaction ID to the selected carbon source
	lista_carbon_sources=['EX_ETOH_e0','stdev'] #This can be changed for a different carbon source. Use with Flux sampling
	carbon_sources=['EX_cpd00363_e0']	#Use reaction ID from the selected carbon source. Use with Flux sampling
	#model.reactions.rxn10042_c0.bounds=[-1000,1000] #Enable constraint with alternative carbon sources (no ethanol)
	#model.reactions.rxn08985_c0.bounds=[-0,0] #Enable constraint with alternative carbon sources (no ethanol)
	#model.reactions.rxn05209_c0.bounds=[-0,0] #Enable constraint with alternative carbon sources (no ethanol)
	#model.reactions.rxn00536_c0.bounds=[0,0] #Ethanol oxidation NADPH dependent (with ethanol as carbon source)
	model.reactions.rxn00062_c0.bounds=[8.4,12] #ATPM
	print(model.optimize()) #Comment the rest below if we only use FBA. Comment when using Flux sampling
	print(model.summary()) #Comment the rest below if we only use FBA. Comment when using Flux sampling 
	# print(len(model.metabolites)) #Model composition
	# print(len(model.reactions)) #Model composition
	# print(len(model.genes)) #Model composition
	#essential_genes,essential_reactions=essentiality_test(model) #Uncomment to run essentiality test
	#print(essential_genes,essential_reactions)
	y=[] #List to store the average fluxes and stdev afer running Flux sampling
	for source in carbon_sources:
		if source=='EX_cpd00363_e0':
			print(source)
			model.reactions.Rnf_c0.upper_bound=0
			model.reactions.Rnf_c0.lower_bound=-1000
			model.reactions.rxn10042_c0.lower_bound=-1000
			model.reactions.rxn10042_c0.upper_bound=0			
			model.reactions.rxn05938_c0.upper_bound=1000
			model.reactions.rxn05938_c0.lower_bound=-1000
			model.reactions.EX_cpd00011_e0.lower_bound=-20 #We fix the CO2 to simulate the theoretical yield
			model.reactions.EX_cpd00011_e0.upper_bound=-20	
			model.reactions.get_by_id(source).lower_bound=-30
			model.reactions.get_by_id(source).upper_bound=-30		
			solution=model.optimize()
			#print(model.summary())
			model.reactions.BIOMASS_Aneopro_w_GAM.lower_bound=0.99*solution.fluxes['BIOMASS_Aneopro_w_GAM']
		elif 'EX_cpd00363_e0_EX' in source:
			print(source)
			if source=='EX_cpd00363_e0_EX_cpd00035_e0': #Ethanol+ Alanine
				model.reactions.EX_cpd00363_e0.lower_bound=-20
				model.reactions.EX_cpd00363_e0.upper_bound=-20
				model.reactions.EX_cpd00035_e0.lower_bound=-10
				model.reactions.EX_cpd00035_e0.upper_bound=-10
				model.reactions.EX_cpd00011_e0.upper_bound=1000
				model.reactions.EX_cpd00011_e0.lower_bound=-1000
				model.reactions.rxn05938_c0.lower_bound=-1000
				model.reactions.rxn05938_c0.upper_bound=1000	
				model.reactions.rxn10042_c0.lower_bound=-1000
				model.reactions.rxn10042_c0.upper_bound=0					
				solution=model.optimize()
				print(model.summary())
				model.reactions.BIOMASS_Aneopro_w_GAM.lower_bound=0.99*solution.fluxes['BIOMASS_Aneopro_w_GAM']	
			else:
				if source=='EX_cpd00363_e0_EX_cpd00029_e0': #Ethanol + Acetate
					model.reactions.EX_cpd00363_e0.lower_bound=-30
					model.reactions.EX_cpd00363_e0.upper_bound=-20
					model.reactions.EX_cpd00029_e0.lower_bound=-10 #Acetate could be produced or/and consumed when acetate is present in the media.
					model.reactions.EX_cpd00029_e0.upper_bound=1000
					model.reactions.EX_cpd00011_e0.upper_bound=1000 #We do not force the uptake of CO2 in this scenario.
					model.reactions.EX_cpd00011_e0.lower_bound=-20
					# model.reactions.rxn05938_c0.lower_bound=0
					# model.reactions.rxn05938_c0.upper_bound=1000	
					model.reactions.rxn10042_c0.upper_bound=0
					model.reactions.rxn10042_c0.lower_bound=-1000					
					solution=model.optimize()
					print(model.summary())
					model.reactions.BIOMASS_Aneopro_w_GAM.lower_bound=0.99*solution.fluxes['BIOMASS_Aneopro_w_GAM']	
				else:				
					if source=='EX_cpd00363_e0_EX_cpd00054_e0': #Ethanol + serine
						model.reactions.EX_cpd00363_e0.lower_bound=-20
						model.reactions.EX_cpd00363_e0.upper_bound=-20
						model.reactions.EX_cpd00054_e0.lower_bound=-10
						model.reactions.EX_cpd00054_e0.upper_bound=-10
						model.reactions.EX_cpd00011_e0.upper_bound=1000
						model.reactions.EX_cpd00011_e0.lower_bound=-1000
						model.reactions.rxn05938_c0.lower_bound=-1000
						model.reactions.rxn05938_c0.upper_bound=1000	
						model.reactions.rxn10042_c0.upper_bound=0
						model.reactions.rxn10042_c0.lower_bound=-1000		
						solution=model.optimize()
						print(model.summary())
						model.reactions.BIOMASS_Aneopro_w_GAM.lower_bound=0.99*solution.fluxes['BIOMASS_Aneopro_w_GAM']					
					elif source=='EX_cpd00363_e0_EX_cpd00156_e0': #Ethanol + valine
						model.reactions.EX_cpd00363_e0.lower_bound=-27
						model.reactions.EX_cpd00363_e0.upper_bound=-27
						model.reactions.EX_cpd00156_e0.lower_bound=-3
						model.reactions.EX_cpd00156_e0.upper_bound=-3
						model.reactions.EX_cpd00011_e0.upper_bound=1000
						model.reactions.EX_cpd00011_e0.lower_bound=-1000
						model.reactions.rxn05938_c0.lower_bound=-1000
						model.reactions.rxn05938_c0.upper_bound=1000	
						model.reactions.rxn10042_c0.upper_bound=0
						model.reactions.rxn10042_c0.lower_bound=-1000		
						solution=model.optimize()
						print(model.summary())
						model.reactions.BIOMASS_Aneopro_w_GAM.lower_bound=0.99*solution.fluxes['BIOMASS_Aneopro_w_GAM']	
					else:  #Ethanol + Leucine
						model.reactions.EX_cpd00363_e0.lower_bound=-28
						model.reactions.EX_cpd00363_e0.upper_bound=-28
						model.reactions.EX_cpd00107_e0.lower_bound=-2
						model.reactions.EX_cpd00107_e0.upper_bound=-2
						model.reactions.EX_cpd00011_e0.upper_bound=1000
						model.reactions.EX_cpd00011_e0.lower_bound=-1000
						model.reactions.rxn05938_c0.lower_bound=-1000
						model.reactions.rxn05938_c0.upper_bound=1000	
						model.reactions.rxn10042_c0.upper_bound=0
						model.reactions.rxn10042_c0.lower_bound=-1000		
						solution=model.optimize()
						print(model.summary())
						model.reactions.BIOMASS_Aneopro_w_GAM.lower_bound=0.99*solution.fluxes['BIOMASS_Aneopro_w_GAM']	
						
		else:	
			print(source) #For alternative carbon sources
			model.reactions.get_by_id(source).lower_bound=-30
			model.reactions.get_by_id(source).upper_bound=-30
			model.reactions.EX_cpd00029_e0.lower_bound=0
			model.reactions.EX_cpd00029_e0.upper_bound=1000			
			model.reactions.EX_cpd00011_e0.upper_bound=1000
			model.reactions.EX_cpd00011_e0.lower_bound=0
			model.reactions.rxn05938_c0.lower_bound=-1000
			model.reactions.rxn05938_c0.upper_bound=1000	
			model.reactions.rxn10042_c0.lower_bound=0	
			model.reactions.rxn10042_c0.upper_bound=1000
			model.reactions.rxn08985_c0.bounds=[-0,0] #To avoid futile cycles
			model.reactions.rxn05209_c0.bounds=[-0,0] #To avoid futile cycles							
				# model.reactions.EX_AC_t0.lower_bound=0
				# model.reactions.EX_AC_t0.upper_bound=0
				# model.reactions.trans_ac_c0.lower_bound=0
				# model.reactions.trans_ac_c0.upper_bound=0
				# model.reactions.trans_ac_e0.lower_bound=0
				# model.reactions.trans_ac_e0.upper_bound=0				
			solution=model.optimize()
			print(model.summary())		
			model.reactions.BIOMASS_Aneopro_w_GAM.lower_bound=0.99*solution.fluxes['BIOMASS_Aneopro_w_GAM']						
		#Flux sampling
		number_samples=5000
		s = sample(model, number_samples, method="achr")
		suma=0.0
		average=0.0
		standard_deviation=0.0
		reactions=[]
		flux=[]
		stdev=[]	
		lista_samples=[]
		for col in s.columns:
			columnsData = s.loc[ : , col]
			suma=0
			total=0
			lista_samples=[]
			for i in range(len(columnsData)):
				suma=suma + float(columnsData[i])
				total=total+1
				lista_samples.append(float(columnsData[i]))
			average=float(round(suma/total,4))
			standard_deviation=statistics.stdev(lista_samples)
			reactions.append(col)
			flux.append(average)
			stdev.append(standard_deviation)
		if 'EX_cpd00363_e0_' in source:
			model.reactions.EX_cpd00363_e0.upper_bound=0
			model.reactions.EX_cpd00363_e0.lower_bound=0
			model.reactions.EX_cpd00035_e0.upper_bound=0
			model.reactions.EX_cpd00035_e0.lower_bound=0
			model.reactions.EX_cpd00054_e0.upper_bound=0
			model.reactions.EX_cpd00054_e0.lower_bound=0
			model.reactions.EX_cpd03559_e0.upper_bound=1000
			model.reactions.EX_cpd03559_e0.lower_bound=0			
			model.reactions.EX_cpd00029_e0.upper_bound=1000
			model.reactions.EX_cpd00029_e0.lower_bound=0
			# model.reactions.EX_AC_t0.lower_bound=0
			# model.reactions.EX_AC_t0.upper_bound=0
			# model.reactions.trans_ac_c0.lower_bound=0
			# model.reactions.trans_ac_c0.upper_bound=0
			# model.reactions.trans_ac_e0.lower_bound=0
			# model.reactions.trans_ac_e0.upper_bound=0			
		else:	
			if 'EX_cpd03559_e0_' in source:
				model.reactions.EX_cpd03559_e0.upper_bound=1000
				model.reactions.EX_cpd03559_e0.lower_bound=0
				model.reactions.EX_cpd00029_e0.upper_bound=1000
				model.reactions.EX_cpd00029_e0.lower_bound=0
				model.reactions.EX_cpd00363_e0.upper_bound=0
			else:
				model.reactions.get_by_id(source).upper_bound=0	
				model.reactions.get_by_id(source).lower_bound=0	
		y.append(flux)
		y.append(stdev)
	data={}
	for i in range(len(lista_carbon_sources)):
		label=lista_carbon_sources[i]
		data[label]=y[i]
	df1 = DataFrame(data,index = reactions)
	now = datetime.now()	
	name = 'Flux sampling'
	writer = pd.ExcelWriter('FluxSampling_PhenotypesProductProfile_etoh_ac.xlsx', engine='openpyxl')
	df1.to_excel(writer, sheet_name=name)	
	writer.save()
	writer.close()		
