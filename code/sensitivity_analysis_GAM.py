#!/usr/bin/env python

"""
Publication:"Genome-scale metabolic modelling enables deciphering ethanol metabolism 
via the acrylate pathway in the propionate-producer Anaerotignum neopropionicum"

Author: Sara Benito Vaquerizo
 
Script to perform a sensitivity analysis on the effect of growth rate when varying +/-10% the composition
of the main biomass components within the biomass synthesis reaction of iANEO_SB607. 

The script outputs an excel file and plots with the generated data (Figure S1).

Cobrapy and functions are used from https://github.com/opencobra/m_model_collection/
Python 3.6.9 is used as the programming language

"""
# Import statements
import os
import warnings
import re
from itertools import chain
from sys import argv

import sympy
import scipy
import scipy.io


import cobra
import cobra.test

from cobra import Model, Reaction, Metabolite
import pandas
from cobra.util.solver import linear_reaction_coefficients

import numpy as np
import pandas as pd
from contextlib import suppress
from cobra.flux_analysis import (
    single_gene_deletion, single_reaction_deletion, double_gene_deletion,
    double_reaction_deletion)
import matplotlib.pyplot as plt
from cobra.medium import minimal_medium
from cobra.test import create_test_model

import multiprocessing
import multiprocessing.pool
from cobra.flux_analysis import production_envelope
from cobra.flux_analysis import gapfill
import plotly.graph_objects as go
import plotly.express as px
import random

from cobra.sampling import OptGPSampler, ACHRSampler, sample
from cobra.flux_analysis import gapfill

import math
from math import sqrt
from cobra.flux_analysis.loopless import add_loopless, loopless_solution
from cobra.flux_analysis import pfba
import statistics as stats
from statistics import stdev
import optlang
import cobra.util.solver as sutil
import plotly.graph_objects as go
import plotly.express as px
from cobra.flux_analysis.loopless import add_loopless, loopless_solution
from cobra.flux_analysis import pfba
import statistics


	
def add_biomass(Model,x):
	"""
	Addition of the new biomass reaction with the change of the 
	proteins, plipids and cell wall stoichiometry after randomly selecting
	the values of proteins and plipids among a 10% range the original
	value

	Input:
	model: model, cobrapy model structure
	x,y,z: proteins, lipids and cell walls stoichiometry

	Outpu:
	model, cobrapy model structure with the added pathway
	"""

# Define the new biomass synthesis reaction

	r1 = Reaction("changed_biomass")
	r1.name = "Biomass_test"
	r1.subsystem = "Biomass"
	r1.lower_bound = 0.  # This is the default
	r1.upper_bound = 1000. # This is the default
	r1.add_metabolites({ model.metabolites.get_by_id("cpd00002_c0"): -x,
	model.metabolites.get_by_id("cpd00001_c0"):-x,
	model.metabolites.get_by_id("teich_met_c0"): -0.1106,
	model.metabolites.get_by_id("peptido_met_c0"): -0.1008,
	model.metabolites.get_by_id("carbo_met_c0"): -0.0432,
	model.metabolites.get_by_id("dna_met_c0"): -0.026,
	model.metabolites.get_by_id("plipid_met_c0"): -0.076,
	model.metabolites.get_by_id("protein_met_c0"): -0.5284,
	model.metabolites.get_by_id("rna_met_c0"): -0.0655,
	model.metabolites.get_by_id("trace_met_c0"): -0.0495,
	model.metabolites.get_by_id("cpd11416_c0"): +1.0,
	model.metabolites.get_by_id("cpd00008_c0"): +x,
	model.metabolites.get_by_id("cpd00009_c0"): +x,
	model.metabolites.get_by_id("cpd00067_c0"): +x})


	model.add_reactions({r1})
	
	return model


	
if __name__ == "__main__":
	model=cobra.io.read_sbml_model("iANEO_SB607.sbml")
	model.reactions.EX_cpd00363_e0.bounds=[0,0]
	model.reactions.EX_cpd00363_e0.lower_bound=-30
	model.reactions.EX_cpd00363_e0.upper_bound=-30
	model.reactions.EX_cpd00011_e0.lower_bound=-20
	model.reactions.EX_cpd00011_e0.upper_bound=-20
	model.reactions.EX_cpd00084_e0.lower_bound=-0.01
	model.reactions.EX_cpd00084_e0.upper_bound=-0.001
	model.reactions.rxn05209_c0.bounds=[-1000,1000]
	model.reactions.rxn08985_c0.bounds=[-1000,1000]
	model.reactions.rxn10042_c0.bounds=[-1000,0]
	model.objective="BIOMASS_Aneopro_w_GAM"
	solution=model.optimize()
	print(model.summary())
	model.reactions.BIOMASS_Aneopro_w_GAM.knock_out()
	# Protein_met=0.5284
	# cellwall_met=0.2546
	# plipid_met=0.076
	valores=[]
	valores2=[]
	for i in range(1000):
		GAM=round(random.uniform(32.0, 48.0),4)
		#print(Protein_met,plipid_met,cellwall_met)
		with model:
			add_biomass(model, GAM)
			model.reactions.EX_cpd00363_e0.lower_bound=-30
			model.reactions.EX_cpd00363_e0.upper_bound=-30
			model.reactions.EX_cpd00011_e0.lower_bound=-20
			model.reactions.EX_cpd00011_e0.upper_bound=-20
			model.reactions.EX_cpd00084_e0.lower_bound=-0.01
			model.reactions.EX_cpd00084_e0.upper_bound=-0.001
			model.objective="changed_biomass"
			solution2=model.optimize()
			valores2.append((GAM,solution2.fluxes['changed_biomass']))
			#print(solution.fluxes['BIOMASS_Aneopro_w_GAM'])
			valores.append((GAM,(solution2.fluxes['changed_biomass']-solution.fluxes['BIOMASS_Aneopro_w_GAM'])))			
						
	listaGAM=[]
	listagrowth=[]
	listaGAM2=[]
	listagrowth2=[]
	for i in range(len(valores)):
		listaGAM.append(valores[i][0])
		listagrowth.append(valores[i][1])
	for i in range(len(valores2)):
		listaGAM2.append(valores2[i][0])
		listagrowth2.append(valores2[i][1])		
		
	
	#Generate excel file
	
	df = pd.DataFrame(valores2, columns =['GAM (g/mmol)','Growth (1/h)'])
	writer = pd.ExcelWriter('Sensitivity_analysis_GAM.xlsx', engine='openpyxl')
	name = 'Sensitivity_analysis'
	df.to_excel(writer, sheet_name=name)	
	writer.save()
	writer.close()	
	#Plot data
	fig = plt.figure()
	plt.scatter(listaGAM, listagrowth, marker='o',alpha=0.7,color="green",label="{}".format('GAM'))
	plt.ylabel('Difference in growth rate (1/h)',fontsize=12)
	plt.xlabel('GAM (g/mmol)',fontsize=12)
	plt.legend()
	fig.savefig('sensi_GAM.png',bbox_inches='tight')	
	plt.show()

