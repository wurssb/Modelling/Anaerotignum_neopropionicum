# Aneo-GEM: The consensus genome-scale metabolic model of _Anaerotignum neopropionicum_


# Description

This repository contains the current consensus genome-scale metabolic model of _Anaerotignum neopropionicum_. 

# Citation

* If you use Aneo-GEM please cite the original publication:
'Genome-scale metabolic modelling enables deciphering ethanol metabolism via the acrylate pathway in the propionate-producer _Anaerotignum neopropionicum_' (https://microbialcellfactories.biomedcentral.com/articles/10.1186/s12934-022-01841-1).

  
# Keywords

**Utilisation:** experimental data reconstruction; omics integrative analysis; _in silico_ strain design; model template  
**Field:** metabolic-network reconstruction  
**Type of model:** reconstruction; curated   
**Omic source:** genomics;  
**Taxonomic name:** _Anaerotignum neopropionicum_  
**Taxonomy ID:** [taxonomy:1121327](https://identifiers.org/taxonomy:1121327)  
**Genome ID:** [insdc.gca:GCA_001571775.1](https://identifiers.org/insdc.gca:GCA_001571775.1)  
**Metabolic system:** general metabolism  
**Strain:** DSM 3847 
**Condition:** anaerobic  

## Model overview

| Taxonomy | Latest update | Version | Reactions | Metabolites | Genes |
|:-------|:--------------|:------|:------|:----------|:-----|
| _Anaerotignum neopropionicum_ | 23-03-2023 | 1.1.0 | 934 | 816 | 607 |

### Gene essentiality prediction

See FROG report (https://www.ebi.ac.uk/biomodels/models)

### Growth prediction

See FROG report (https://www.ebi.ac.uk/biomodels/models)


## Obtain model

You can obtained the model by any of the following methods:

1. The model can also be accessed from BioModels (https://www.ebi.ac.uk/biomodels/models) under the following identifier: MODEL2201310001.
2. If you have a Git client installed on your computer, you can clone the [`main`](https://gitlab.com/-/ide/project/wurssb/Modelling/Anaerotignum_neopropionicum) branch of the Aneo-GEM repository.


## Required software

### Basic user

If you want to use the model for your own model simulations, you can use **any software** that accepts SBML L3V1 FBCv3 formatted model files. This includes any of the following:
* MATLAB-based
  * [RAVEN Toolbox](https://github.com/SysBioChalmers/RAVEN) version 2.7.1 or later (recommended)  
  * [COBRA Toolbox](https://github.com/opencobra/cobratoolbox)

* Python-based
  * [cobrapy](https://github.com/opencobra/cobrapy)  

Please see the installation instructions for each software package.


# Contributing

Contributions are always welcome! Please read the contributions guideline to get started (contributing.md).







