ANEO-GEM 1.1.0:

- Fixes: 
     - Added metabolite cpd00023_e0 (Glutamate extracellular)
     - Added the glutamate transport reaction rxn05146_c0 
     - Added the extracellular glutamate reaction EX_cpd00023_e0
     - Corrected transport reactions rxn10447_c0 and rxn05555_c0 (metabolite cpd00001_e0 changed to cpd00001_c0)
       

