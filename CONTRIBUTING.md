# Contributor guidelines

We welcome contributions from everyone and appreciate your consideration in helping us to improve our model.


## Reporting issues in the model

Before suggesting an issue. please look over our milestones and issue boards to see whether your feature has already been listed.

Report an issue if you note any of the following:

Incorrect annotation for any model component.
Missing feature or field you would like the model to have.
Bug/weird simulation results.
Lacking documentation.
Any type of feedback.

To report an issue:

1. Go to the project.

2. Click the Issues button on the left of the window.

3. Click the "New issue" button.

4. Fill out the Title and Description. You don't need to fill out the other fields.

When creating the issue, please make sure:

You tested your code (if any) with all requirements for running the model.
You did your analysis in the main branch of the repository.
You provide any necessary files/links needed for understanding the issue.
You checked that a similar issue does not exist already
Feel free to also comment on any of the open issues. When doing so, please comply with our code of conduct.

After you create an issue, our team will assign a priority to the issue, ensure it is labeled properly, and perhaps request additional information from you.


## Contributing to the model

Do you want to contribute to the model with some additions or improvements? Consider starting by raising an issue and assign it to yourself to describe what you want to achieve. This way, we reduce the risk of duplicated efforts and you may also get suggestions on how to best proceed, e.g. there may be half-finished work in some branch that you could start with. Also, feel free to browse our open issues and our ongoing projects: Anything tagged with "help wanted" is open to whoever wants to implement it!

## Cloning, Branching, and Merging

Anyone contributing source code should clone the project (also known as *forking*) using Git commands. The term *fork* in GitLab differs from an older meaning of the word, which referred to people breaking away from a project and starting a new one. Here, *fork* just means that someone creates their own personal copy, and intends to push changes back to the project.

Git also allows multiple branches of a project. 

Submitting code to a project is called a *pull request*. A comprehensive [Gitlab document](https://docs.gitlab.com/ee/topics/gitlab_flow.html) describes various robust workflows for handling pull requests. We recommend that you adopt one of those workflows.

Although most pull requests fix bugs or add features, a pull request can offer more specific contributions as well, such as a test for a bug or a feature.

### Style guide

### Pull request workflow

The [Introduction to GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) provides useful background. Please use the following workflow when submitting merge requests.

### Creating the material for a pull request

1. Fork our project into your personal namespace or group.

2. Create a feature branch in your fork. If possible, include the title of the issue or the issue number that the merge request corresponds to.

3. Write appropriate continuous integration tests.

4. Push the commit(s) to your working branch in your fork.
  - Squash commits into a small number of logically organized commits, keeping the commit history intact on shared branches.
  - Please try to have fewer than 15 commit messages per merge request.

### Testing

### The merge request

Submit a merge request (MR) to our projects default branch, following these criteria.

  - Refer to the issue that defines the bug/issue you are fixing or the feature you are adding.
  - Include a test or tests.
  - Include updates to any relevant or affected documentation.
  - The merge request title should describe the change you wish to make.
  - The merge request description should give the reason for the change.
  - Use complete URLs to issues and other merge requests, milestones, etc.
  - Include the complete path when referring to files within this repository (no URL required).
  - Try to keep the amount of changes you make in a single merge request as small as possible.
  - Do not include emoji in commit messages.

Please follow Chris Beam's seven rules for writing a good commit messages:

- Separate subject from body with a blank line.
- Limit the subject line to 50 characters.
- Capitalize the subject line.
- Do not end the subject line with a period.
- Use the imperative mood in the subject line.
- Wrap the body at 72 characters.
- Use the body to explain what and why versus how.

For examples and additional explanation of these seven rules, please read [Chris Beam's blog post](https://chris.beams.io/posts/git-commit/).

Some examples:

commit	commit message
Add new rxns	feat-rxn: methanol pathway
Remove a metabolite	fix-met: duplicated citrate
Add metabolite formula	feat-met.prop: carbohydrate formulas
Fix rxn stoichiometry	fix-rxn.prop: complex V stoich coeffs
Update gene IDs	fix-gene.annot: update IDs from SWISSPROT
Format name of compartment	style-comp.annot: remove uppercases
Split a rxn in 2	refactor-rxn: split isomerase in 2 steps
Add some data	feat-data: metabolomics data
Update documentation of function	doc: addDBnewRxn.m
Update toolbox	chore: update RAVEN version

As much as possible follow the model namespace:
For metabolite and/or reaction names, please avoid any unconventional characters (e.g. Greek letters).
For new genes, please use the original id: CLNEO_XXXXX.

Additions/modifications to the model should be applied on the last model version in table (.xlsx) and sbml format. The rest of model formats can be updated using COBRApy by reading the sbml file and saving it as yaml, mat and json.

### Merge request acceptance criteria

Your merge request needs at least one approval from a maintainer on the project, but depending on your change you might need additional approvals. Merge requests that fix a regression are usually accepted quickly and usually only require a single approval, whereas a new feature may require approval from multiple maintainers and our project leads.

### Reviewing pull requests

Every pull request must be approved by at least one reviewer before it can be merged. When reviewing someone else's pull request, keep in mind the following aspects:

Compatibility: First of all, make sure that the model is still compatible and that no errors appear. Check also that does not change in any unexpected ways (e.g. an "unknown" toolbox version). Finally, ensure that the SBML fields model metaid, model id and model name never change, as if they change it would create a conflict in the next release.
Documentation: Every change should be justified with a reference/link/argument. This can be provided as data in /data, or directly as a comment in the pull request.
Style: Ensure that the changes to the model are compliant with the model's rxn/met/gene naming conventions (when unsure, take a look at a similar field in the model). Also, make sure that scripts have a compliant style, and datasets are straight-forward to understand.
When commenting in the review, please comply with our code of conduct.
Avoid vague comments and try to be as explicit as possible (e.g.: "Please include X here" instead of "X could be included here").
As much as possible, try to keep the review process in the pull request discussion, and not in separate private emails.

### Releasing a new version
A merge of develop with main invokes a new release.

A new release should be made as soon as there is substantial new work in develop (as rule of thumb, after around 3 pull request merges).
Aneo-GEM follows semantic versioning, adapted to GEMs:

A major release is seldom used and only meant for a new publication. Backwards compatibility should be, ideally, always preserved.
A minor release involves a substantial change in the model (several new reactions/metabolites/genes), such as:
Addition of genes/reactions/metabolites from a whole genome annotation.
Addition of several annotation fields.
Inclusion of a major new formalism in the model.
Addition of a plurality of pathways.

A patch release is the most common one and is done when only few things have changed in the model, or there are only changes that have to do with format, such as:
Adding a single new annotation field.
Fixing some chemical formulas/charges.
Updating toolboxes.
Re-organization of data
Refactoring of code.

When releasing, please follow these steps:

Create a pull request from develop to main, indicating all new features/fixes/etc. and referencing every previous pull request included (examples here). Tip: if any issue gets solved in the release, write in the pull request description "Closes #X", where "X" is the issue number. That way the issue will be automatically closed after merge.

Merge at least a day after (having at least one accepted review).

The name of the models should have the last version at the end in all the model formats (e.g, iANEO_SB607_X.Y.Z)

Switch locally to main, pull changes and update history.md, by putting at the top the same description of the corresponding pull request.

Update the version in the file version.txt. 

## Non-code contributions

## Code of conduct

Please read the [IEEE Code of Conduct](https://www.ieee.org/content/dam/ieee-org/ieee/web/org/about/ieee_code_of_conduct.pdf), which governs projects on IEEE SA OPEN.

